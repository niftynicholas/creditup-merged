import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import { Invoice } from '../invoice';
import { Globals } from '../globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  invoices: Invoice[];
  selectedInvoice: Invoice;
  paymentDueDate: string;
  clientName: string;
  clientEmail: string;
  jobDesc: string;
  total: number;

  constructor(private globals: Globals, private router: Router) { 
    this.invoices = globals.invoices;
  }

  ngOnInit() {
  }

  onClick() {
    this.invoices.push({id: this.invoices.length + 1, paymentDueDate: this.paymentDueDate, clientName: this.clientName, clientEmail: this.clientEmail, jobDesc: this.jobDesc, total: this.total});
    this.globals.invoices = this.invoices;
    this.paymentDueDate = '';
    this.clientName = '';
    this.clientEmail = '';
    this.jobDesc = '';
    this.total = 0.00;
  }

  onSelect(invoice: Invoice): void {
    this.selectedInvoice = invoice;
    localStorage.setItem('selectedInvoice', this.selectedInvoice.id + '');
    this.router.navigateByUrl('/viewinvoice');
  }
}