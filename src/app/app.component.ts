import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router  , ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  url: String;

  constructor(title: Title, router: Router) {
     title.setTitle('Credit Up');
     this.url=location.pathname;
     console.log(this.url);
     
  }

  ngOnInit() {
  }

  showHeader(){
      if(this.url == "/login" || this.url == "/"){
           return false;
       }else{
           return true;
       }
  }
}

