import { Injectable } from '@angular/core';
import { INVOICES } from './mock-invoices';

@Injectable()
export class Globals {
  invoices = INVOICES;  
}