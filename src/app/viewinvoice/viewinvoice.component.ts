import { Component, OnInit } from '@angular/core';
import { Invoice } from '../invoice';
import { Globals } from '../globals';

@Component({
  selector: 'app-viewinvoice',
  templateUrl: './viewinvoice.component.html',
  styleUrls: ['./viewinvoice.component.css']
})
export class ViewinvoiceComponent implements OnInit {
  invoices: Invoice[];
  selectedInvoice: Invoice;
  id: number;  

  constructor(private globals: Globals) { 
  	this.invoices = globals.invoices;
  }

  ngOnInit() {
  	this.id = Number(localStorage.getItem('selectedInvoice')); // convert to number
  	this.selectedInvoice = this.invoices[this.id-1];
  }
}
