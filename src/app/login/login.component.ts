import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  background = 'assets/assets/images/background/bg-1.jpg';
  user = 'assets/images/Citi.png';

  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(assets/assets/images/background/bg-1.jpg)`);
}

}
