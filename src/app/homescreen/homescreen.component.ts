import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-homescreen',
  templateUrl: './homescreen.component.html',
  styleUrls: ['./homescreen.component.css']
})

export class HomeScreenComponent implements OnInit {

  title = 'Credit Cards';
  banner = 'assets/images/Citi.png';
  imageroute = 'assets/images/'; 

  constructor() { }

  ngOnInit() { }
}
