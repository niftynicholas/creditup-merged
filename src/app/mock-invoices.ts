import { Invoice } from './invoice';

export const INVOICES: Invoice[] = [
  { id: 1, clientName: 'Mr. Nice', clientEmail: 'mrnice@gmail.com', paymentDueDate: '09/08/2018', jobDesc: 'Logo design', total: 120},
  { id: 2, clientName: 'Mr. Angry', clientEmail: 'mrangry@yahoo.com', paymentDueDate: '10/08/2018', jobDesc: 'Flyer design', total: 250},
  { id: 3, clientName: 'Mr. Sad', clientEmail: 'mrsad@hotmail.com',paymentDueDate: '11/08/2018', jobDesc: 'Banner design', total: 300}
];